package inlblackjack;
// Parprogrammering
import java.util.ArrayList;

public class Blackjack {
	
	private static Blackjack bj = new Blackjack(); 
	
	private ArrayList<BJCard> hand = new ArrayList<>();
	private ArrayList<BJCard> dealer = new ArrayList<>();
	private BJDeck deck = new BJDeck();
	
	BJCard tempcard;
	
	private Blackjack() {
	}
	
	public static Blackjack getInstance() {
		return Blackjack.bj;
	}
	
	public void hit() {
//		temporary card 
		tempcard = deck.draw();
		
		hand.add(tempcard);
		
		System.out.println("Du fick "+ tempcard.toString()+ " ("+ score(hand) +")");
	}
	
	public void stand() {
		System.out.println("Din poäng: "+ score(hand));
		dealerPlays();
		System.out.println("Dealer spelar...");		
//		Spelaren vinner
		if((score(hand)== 21 && score(dealer) != 21)
			|| (score(hand) < 21 && (score(dealer) > 21 || score(hand) > score(dealer)))) {
			System.out.println("Du fick " + score(hand) + ", " + showAllCards(hand) + 
					"\nDealer har "+ score(dealer) + ", " + showAllCards(dealer) +  
					 "\nDu vinner!\n");
		}
//		Oavgjort
		else if((score(hand) == score(dealer)) 
				|| (score(hand) > 21 && score(dealer) > 21)) {
			System.out.println("Du fick " + score(hand) + ", " + showAllCards(hand) + 
								"\nDealer har "+ score(dealer) + ", " + showAllCards(dealer) +  
								 "\nDet är oavgjort!\n");
		}
//		Dealer vinner
		else {
			System.out.println("Du fick " + score(hand) + ", " + showAllCards(hand) + 
					"\nDealer har "+ score(dealer) + ", " + showAllCards(dealer) +  
					 "\nDu förlorar!\n");
		}
	}
	
	public void reset() {
//		if(hand.size() != 0) {
//			while(hand.size() != 0){
//				hand.remove(0);
//			}
//			while(dealer.size() != 0){
//				dealer.remove(0);
//			}
//		}
//		Istället av if-satsen
		hand.clear();
		dealer.clear();
		
		deck = new BJDeck();
		deck.shuffle();

		for(int i = 0; i < 2; i++) {
			BJCard playercard = deck.draw();
			hand.add(playercard);
			BJCard dealercard = deck.draw();
			dealer.add(dealercard);
		}
		
		System.out.println("Du har "+ showAllCards(hand) + " (" + score(hand) + ")");
		System.out.println("Dealer har "+ dealer.get(0).toString());
	}

	private int score(ArrayList<BJCard> hand) {

		int score = 0;

		for(int i = 0; i < hand.size(); i++) {
			if(hand.get(i).getValue() >=2 && hand.get(i).getValue() <= 10) {
				score += hand.get(i).getValue();
			}
			else if(hand.get(i).getValue() >= 11 && hand.get(i).getValue() <= 13 ) {
				score += 10;
			}
			else if(hand.get(i).getValue() == 1) {
				score += 11;
			}
		}
		for(int i = 0; i < hand.size(); i++) {
			if(hand.get(i).getValue() == 1 && score > 21) {
				score = score - 10;
			}
		}
		return score;
	}
//	Delarens spelmetod
	private void dealerPlays() {
		while(score(dealer) < 17) {
			dealer.add(deck.draw());
		}
	}
//	Metod som returnerar alla kort på handen. (delarens eller spelarens)
	public String showAllCards(ArrayList<BJCard> cards) {
		String allCards = "";
		String eachCard;
		for(int i = 0; i < cards.size() - 1; i++) {
			eachCard = cards.get(i).toString();
			allCards += eachCard + ", ";
		}
		eachCard = cards.get(cards.size() - 1).toString();
		allCards += eachCard;
		return allCards;
	}
	
//	ändring för JUnit test
	public int scoreForTest(ArrayList<BJCard> hand) {

		int score = 0;

		for(int i = 0; i < hand.size(); i++) {
			if(hand.get(i).getValue() >=2 && hand.get(i).getValue() <= 10) {
				score += hand.get(i).getValue();
			}
			else if(hand.get(i).getValue() >= 11 && hand.get(i).getValue() <= 13 ) {
				score += 10;
			}
			else if(hand.get(i).getValue() == 1) {
				score += 11;
			}
		}
		for(int i = 0; i < hand.size(); i++) {
			if(hand.get(i).getValue() == 1 && score > 21) {
				score = score - 10;
			}
		}
		return score;
	}

}
