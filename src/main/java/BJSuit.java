package inlblackjack;
// Parprogrammering
public enum BJSuit {
	
	HEARTS("H"), SPADES("S"), DIAMONDS("D"), CLUBS("C");
	
	private final String initial;
	
	BJSuit(String initial){
		this.initial = initial;
	}
	
	public String getInitial() {
		return this.initial;
	}

}
