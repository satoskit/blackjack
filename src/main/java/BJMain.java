package inlblackjack;
// Parprogrammering
import java.util.Scanner;

public class BJMain {

	public static void main(String[] args) {
		
		boolean endGame = false;
		Scanner sc = new Scanner(System.in);
		String input;
		
		while(!endGame) {
			System.out.println("Starta spel: N, Avsluta spel: Q");
			input = sc.nextLine();
			
			if(input.equalsIgnoreCase("n")) {
				Blackjack.getInstance().reset();
				
				do{
					System.out.println("Hit: H, Stand: S");
					input = sc.nextLine();
					if(input.equalsIgnoreCase("h")) {
						Blackjack.getInstance().hit();
					}
					else if (!input.equalsIgnoreCase("H") && !input.equalsIgnoreCase("S")){
						System.out.println("Fel input, mata in H eller S!\n");
					}
				}
				while(!(input.equalsIgnoreCase("s")));
				Blackjack.getInstance().stand();
			}

			else if(input.equalsIgnoreCase("q")) {
				endGame = true;
			}
			else {
				System.out.println("Fel input, mata in N eller Q\n");
			}
		}
		System.out.println("Spelet avslutas. Välkommen åter!");
		sc.close();
		System.exit(0);

	}

}
