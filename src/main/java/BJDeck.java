package inlblackjack;
// Parprogrammering
import java.util.ArrayList;
import java.util.List;

public class BJDeck {
	
	private List<BJCard> cards = new ArrayList<>();
	private List<BJCard> shuffledCards = new ArrayList<>();
	private int rand;

//	Konstruktor som populerar kortleken.
	public BJDeck() {
		for(int value = 1; value <= 13; value++) {
			for(BJSuit suit : BJSuit.values()) {
				cards.add(new BJCard(value, suit));
			}
		}
	}
//	Metod som delar ett kort från kortleken.
	public BJCard draw() {
		
		BJCard card = cards.get(0);
		cards.remove(card);
		return card;
	}
//	Metod som "blandar" kortleken. 
	public void shuffle() {
		
		while(cards.size() > 0) { 
			rand = (int)(Math.random()* cards.size());
			BJCard removedcard = cards.remove(rand);
			shuffledCards.add(removedcard);
		}
		cards = shuffledCards;
	}
	
//	metoder för JUnit test
	public List<BJCard> getCards() {
		return this.cards;
	}

}
