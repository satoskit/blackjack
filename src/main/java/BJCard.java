package inlblackjack;
// Parprogrammering
public class BJCard {
	
	private int value;
	private BJSuit suit;
	
	public BJCard(int value, BJSuit suit) {
		this.value = value;
		this.suit = suit;
	}
	
	public int getValue() {
		return value;
	}
	
	public BJSuit getSuit() {
		return suit;
	}
//	Returnerar kortets värde med bokstäverna J (för knekt), Q (för dam), K (för kung), A (för ess).
//	Den lägger också till initialen för kortet istället för hela namnet. 
	@Override
	public String toString() {
		
		if(getValue() == 11) {
			return "J" + getSuit().getInitial();
		}
		else if(getValue() == 12) {
			return "Q" + getSuit().getInitial();
		}
		else if(getValue() == 13) {
			return "K" + getSuit().getInitial();
		}
		else if(getValue() == 1) {
			return "A" + getSuit().getInitial();
		}
		return this.getValue() + this.suit.getInitial();
	}

}

// another test
// test for git
