package inlblackjack;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;


public class TestBlackJack {
	
	BJDeck deck;
	
	@BeforeEach
	void setDeck() {
		deck = new BJDeck();
	}
	
	@Test
	void testDeckSize() {
		assertEquals(52, deck.getCards().size(), "Test1");
	}
	
	@Test
	void testShuffle() {
		BJDeck shuffled = new BJDeck();
		shuffled.shuffle();
		assertEquals(deck.getCards().size(), shuffled.getCards().size(), "Test2");
	}
	
	@ParameterizedTest
	@ValueSource(ints = {1, 10, 51})
	void testAfterDraw(int n) {
		deck.shuffle();
		for(int i = 0; i < n; i++) {
			deck.draw();
		}
		assertEquals(52 - n, deck.getCards().size(), "Test3");
	}
	
	@Test
	void testDuplicate() {
		for(int i = 0; i < deck.getCards().size(); i++) {
			for(int j = 1; j < deck.getCards().size(); j++) {
				if((i != j) && (deck.getCards().get(i).equals(deck.getCards().get(j)))) {
					assertNotEquals(deck.getCards().get(i), deck.getCards().get(j), "Test4");
				}
			}
		}
	}
	
	@Test
	void testDeplicateAftershuffle() {
		deck.shuffle();
		for(int i = 0; i < deck.getCards().size(); i++) {
			for(int j = i + 1; j < deck.getCards().size(); j++) {
					assertNotEquals(deck.getCards().get(i), deck.getCards().get(j), "Test5");
			}
		}
	}
	
	@Test
	void testScore1() {
		ArrayList<BJCard> hand = new ArrayList<>();
		BJCard card1 = new BJCard(1, BJSuit.HEARTS);
		BJCard card2 = new BJCard(1, BJSuit.CLUBS);
		BJCard card3 = new BJCard(12, BJSuit.SPADES);
		hand.add(card1);
		hand.add(card2);
		hand.add(card3);
		int score = Blackjack.getInstance().scoreForTest(hand);
		assertEquals(12, score, "Test6");
	}
	
//	Komplettering
	
	@Test
	void testShuffle2() {
		BJDeck shuffled = new BJDeck();
		shuffled.shuffle();
		assertFalse(deck.equals(shuffled), "Deck before and after shuffle");
	}
	
	@Test
	void testNotNull() {
		assertNotNull(deck.getCards().size(), "Deck is not null");
		
		ArrayList<BJCard> hand = new ArrayList<>();
		assertNotNull(hand, "Hand is not null");	
	}
	
}
